//
//  DetailViewController.m
//  Contacts
//
//  Created by kumaran on 23/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize firstNameLabel, lastNameLabel, ageLabel, contact;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.firstNameLabel.text = [contact firstName];
    self.lastNameLabel.text = [contact lastName];
    self.ageLabel.text = [NSString stringWithFormat:@"%d", [contact age]];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
