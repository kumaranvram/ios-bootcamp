//
//  Contact.m
//  Contacts
//
//  Created by kumaran on 23/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "Contact.h"

@implementation Contact

@synthesize firstName, lastName, age;

-(id) initWithFirstName:(NSString *) fname WithLastName:(NSString *)lname WithAge:(int) ageVal {
    self = [super init];
    self.firstName = fname;
    self.lastName = lname;
    self.age = ageVal;
    return self;
}

@end
