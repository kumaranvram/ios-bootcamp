//
//  DetailViewController.h
//  Contacts
//
//  Created by kumaran on 23/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
@interface DetailViewController : UIViewController

@property (weak) IBOutlet UILabel *firstNameLabel;
@property (weak) IBOutlet UILabel *lastNameLabel;
@property (weak) IBOutlet UILabel *ageLabel;

@property (strong) Contact *contact;

@end
