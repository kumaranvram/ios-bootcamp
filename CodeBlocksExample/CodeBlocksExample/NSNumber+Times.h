//
//  NSNumber+Times.h
//  CodeBlocksExample
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Times) 

-(void) times:(void (^)(void))block;

@end
