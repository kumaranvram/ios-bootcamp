//
//  NSNumber+Times.m
//  CodeBlocksExample
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "NSNumber+Times.h"

@implementation NSNumber(Times)

-(void) times:(void (^)(void))block {
    int intValue = [self intValue];
    for(int i=0; i<intValue; i++) {
        block();
    }
}

@end
