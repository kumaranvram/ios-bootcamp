//
//  ViewController.m
//  CodeBlocksExample
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "ViewController.h"
#import "NSNumber+Times.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self iterateArrayWithParameterizedCodeBlock];
    NSNumber *number = @3.91;
    [number times:^{
        NSLog(@"Hello World");
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) iterateArrayWithParameterizedCodeBlock {
    NSArray *arr = @[@"abc", @"def", @"ghi"];
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)  {
        NSLog(@"%@ is present at %d", obj, idx);
        if(idx == 1)
            *stop = YES;
    }];
}

@end
