//
//  ViewController.m
//  JSONReader
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "ViewController.h"
#import "SBJsonParser.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *user;
@property (weak, nonatomic) IBOutlet UILabel *number;
@end

@implementation ViewController

@synthesize responseData, responseString;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self fetchAsync];
//    NSString *data = [self fetchAsync];
//    [self parseJsonAndUpdateUIWithData:data];
}


- (void) parseJsonAndUpdateUIWithData:(NSString *)data {
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSDictionary *parseddata = [jsonParser objectWithString:data];
    [self.status setText:[[parseddata objectForKey:@"status"] description]];
    [self.number setText:[[parseddata objectForKey:@"your_lucky_number"] description]];
    [self.user setText:[[parseddata objectForKey:@"user"] description]];
}

- (NSString *) fetchSync {
    NSURL *url = [[NSURL alloc] initWithString:@"http://localhost:4567/iosbootcamp"];
    NSString  *data = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:nil];
    return data;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchAsync {
    NSURL *myURL = [NSURL URLWithString:@"http://localhost:4567/iosbootcamp"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:myURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Started receiving response");
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"Started receiving data");
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Unable to fetch data");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Succeeded! Received %d bytes of data",[responseData length]);
    NSString *data = [[NSString alloc] initWithData:responseData encoding: NSASCIIStringEncoding];
    [self setResponseString:data];
    [self parseJsonAndUpdateUIWithData:self.responseString];
}
@end
