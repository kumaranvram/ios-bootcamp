//
//  ContactsListViewController.m
//  CoreDataContacts
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "ContactsListViewController.h"
#import "AppDelegate.h"
#import "Contact.h"

@interface ContactsListViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;

@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UITextField *mobile;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *twitter;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirth;

@end

@implementation ContactsListViewController
- (IBAction)saveContact:(id)sender {
    AppDelegate *appDelegate =
    [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    Contact *newContact = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Contact"
                  inManagedObjectContext:context];
    [newContact setFirstName:[self.firstName text]];
    [newContact setLastName:[self.lastName text]];
    [newContact setCompany:[self.company text]];
    [newContact setHomeEmail:[self.email text]];
    [newContact setMobile:[self.mobile text]];
    [newContact setTwitterHandle:[self.twitter text]];
    NSError *error;
    [context save:&error];
    if(nil == error){
        [self showMessage:@"Contact Saved" WithTitle:@"Success"];
        [self clearFields];
    } else {
        [self showMessage:@"I have no idea what went wrong!" WithTitle:@"Error"];
    }

}


-(void) clearFields {
    [self.firstName setText:@""];
    [self.lastName setText:@""];
    [self.company setText:@""];
    [self.dateOfBirth setText:@""];
    [self.email setText:@""];
    [self.mobile setText:@""];
    [self.twitter setText:@""];
}

-(void) showMessage:(NSString *)message WithTitle:(NSString *)title {
    UIAlertView *messageView= [[UIAlertView alloc] initWithTitle:title
                                                      message:message
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [messageView show];
}

- (IBAction)findContact:(id)sender {
    if([[self.email text] isEqualToString:@""]) {
        [self showMessage:@"Email Id required" WithTitle:@"502"];
        return;
    }
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(homeEmail = %@)",[self.email text]];
    [request setPredicate:pred];
    Contact *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    if ([objects count] == 0) {
        [self showMessage:@"No Matches" WithTitle:@"404"];
    } else {
        matches = objects[0];
        [self.firstName setText:[matches firstName]];
        [self.lastName setText:[matches lastName]];
        [self.company setText:[matches company]];
        [self.mobile setText:[matches mobile]];
        [self.email setText:[matches homeEmail]];
        [self.twitter setText:[matches twitterHandle]];
        [self.dateOfBirth setText:[matches birthday]];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)viewTouched:(id)sender {
    for(id view in [sender subviews]) {
        [view resignFirstResponder];
    }
}

@end
