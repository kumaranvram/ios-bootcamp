//
//  Contact.h
//  CoreDataContacts
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Contact : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) NSString * homePhone;
@property (nonatomic, retain) NSString * workPhone;
@property (nonatomic, retain) NSString * homeEmail;
@property (nonatomic, retain) NSString * workEmail;
@property (nonatomic, retain) NSString * twitterHandle;
@property (nonatomic, retain) NSDate * birthday;

@end
