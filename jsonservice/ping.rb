require 'rubygems'
require 'sinatra'
require 'json'

get '/iosbootcamp' do
  content_type :json
  return {:status => 'success', :user => 'foo', :your_lucky_number => 0}.to_json
end
