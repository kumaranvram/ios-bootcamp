//
// Created by Hariharan Thiagarajan on 24/05/13.
// Copyright (c) 2013 TW. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MyClass.h"

@interface MyClass()
@property NSString *myLocalVar;
@end

@implementation MyClass



- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (NSString *)myString {
    if(!_myString) _myString = @"Hello";
    return _myString;
}

- (void)sayHello:(NSString *)message {
    NSLog(message);


    NSLog(@"Hello world %d, %@",@3,message);

}

- (int)addX:(int)x withY:(int)y {
    
    [self.myString release]
    return x + y;
}


+ (void)myClassMethod {

}


- (void) sayHello {
    self.myString = @"Hello";

    self.myLocalVar = @"Test";
}

-(NSString *) description {
    return [NSString stringWithFormat:@"My Class String"];
}

@end