//
//  ObjectBootTests.m
//  ObjectBootTests
//
//  Created by Hariharan Thiagarajan on 05/24/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import "ObjectBootTests.h"
#import "MyClass.h"

@implementation ObjectBootTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    MyClass *myClass = [[MyClass alloc] init];
    [myClass addX:2 withY:3];
    [MyClass myClassMethod];



    myClass.myString = @"test";

}

@end
