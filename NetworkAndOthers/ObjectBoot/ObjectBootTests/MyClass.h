//
// Created by Hariharan Thiagarajan on 24/05/13.
// Copyright (c) 2013 TW. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface MyClass : NSObject

@property NSString *myString;

- (void) sayHello:(NSString*) message;

-(int) addX:(int) x withY:(int) y;

+ (void) myClassMethod;

@end