//
//  main.m
//  NewsTable
//
//  Created by Hariharan Thiagarajan on 05/24/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWAppDelegate class]));
    }

}