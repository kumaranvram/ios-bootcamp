//
//  HTViewController.m
//  BootcampTable
//
//  Created by Hariharan Thiagarajan on 23/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import "HTViewController.h"
#import "HTMyTableCell.h"

@interface HTViewController ()
@property NSArray *osList;
@end

@implementation HTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.osList = @[@"iOS",@"Android",@"Windows",@"Blackberry"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.osList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";

    HTMyTableCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[HTMyTableCell alloc] init];
    }

    cell.name.text = [self.osList objectAtIndex:indexPath.row];
    return cell;
}



@end
