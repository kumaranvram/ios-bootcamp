//
//  HTViewController.h
//  BootcampTable
//
//  Created by Hariharan Thiagarajan on 23/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@end
