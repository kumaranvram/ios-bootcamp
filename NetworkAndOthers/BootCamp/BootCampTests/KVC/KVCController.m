//
// Created by Hariharan Thiagarajan on 23/05/13.
// Copyright (c) 2013 TW. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "KVCController.h"
#import "KVCModel.h"

@implementation KVCController


- (void)registerAnObserver {
    [self.model addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    NSLog(@"I am called with %@ %@ %@ %@",keyPath,object,change,context);
}

@end