
#import <Foundation/Foundation.h>
#import "KVCModel.h"


@interface KVCController : NSObject
@property KVCModel *model;

- (void)registerAnObserver;
@end