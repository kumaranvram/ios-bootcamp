//
// Created by Hariharan Thiagarajan on 23/05/13.
// Copyright (c) 2013 TW. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "PModelProtocol.h"

@interface PController : NSObject<PModelProtocol>
@end