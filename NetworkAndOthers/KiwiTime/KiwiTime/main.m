//
//  main.m
//  KiwiTime
//
//  Created by Hariharan Thiagarajan on 24/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
