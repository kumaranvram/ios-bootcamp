//
//  AppDelegate.h
//  BootNetwork
//
//  Created by Hariharan Thiagarajan on 25/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
