//
//  ViewController.m
//  BootNetwork
//
//  Created by Hariharan Thiagarajan on 25/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//


#import <complex>
#import "ViewController.h"
#import "SBJsonParser.h"


@interface ViewController ()
@property NSMutableData *myResponse;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.myResponse = [[NSMutableData alloc] init];
	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction) getData:(id)sender {
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://search.twitter.com/search.json?q=thoughtworks"]];

    NSURLConnection* con = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    [con start];
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"Received response %d", ((NSHTTPURLResponse*)response).statusCode);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {

    [self.myResponse appendData:data];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSDictionary *dataAsDictionary = [parser objectWithData:data];

    NSLog(@"Output %@",dataAsDictionary);
    NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"IO am getting data %@", stringData);

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
