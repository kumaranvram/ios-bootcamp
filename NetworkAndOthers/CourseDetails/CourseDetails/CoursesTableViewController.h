//
//  CoursesTableViewController.h
//  CourseDetails
//
//  Created by Hariharan Thiagarajan on 24/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoursesTableViewController : UIViewController <UITableViewDelegate>

@end

@interface CoursesTableDSViewController : UIViewController <UITableViewDelegate>

@end